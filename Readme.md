Build

    docker build --force-rm -t piwigo .

Run container

    docker run -d \
        -p 8000:80 \
        -v piwigo-vol:/var/www/piwigo/upload \
        --hostname piwigo \
        --name piwigo \
        piwigo

Configure mariadb and create new piwigo database

    docker exec -it piwigo bash piwigo_database_create.sh
    
Goto site and enter credentials (database/user/password all piwigo) 

    http://HOST_ADDRESS:8000

Once connected to piwigo correctly return to command prompt to configure
certain default settings that can only be applied to database

    docker exec -it piwigo bash piwigo_database_apply_default_settings.sh 

Return to site and navigate to plugins. Activate the following:

    - Admin Tools

    - VideoJS    

Enter container if you wish to look around

    docker exec -it piwigo bash


    
TODO:

- Work out what volumes are needed to persist all critical data
  and backup/restore existing piwigo system.

- DONE. Work out how to automate installation of plugins (think just need
  to download/unzip to plugins folder under piwigo).

- DONE. Add video support including for uploads using relevant plugin.

- DONE. Make more metadata visible by default.

- Configure server so sending email works.

- Set default to friends and family only.

- DONE. Turn off comments.
