FROM ubuntu:16.04
MAINTAINER  Adam Lawrence <alaw005@gmail.com>

# Required for nano to work
ENV TERM xterm

# Install dependencies including database
RUN apt-get update && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y \
            apache2 \
            bsdtar \
            curl \
            dcraw \
            ffmpeg\
            imagemagick \
            libapache2-mod-php \
            mariadb-server \
            mediainfo \
            nano \
            php-curl \
            php-gd \
            php-mbstring \
            php-mysql \
            php-xml \
            wget

# Copy start script and make executable
COPY config/start.sh /start.sh
RUN chmod +x /start.sh

# Add script that can be run using docker exec to create database 
COPY config/piwigo_database_create.sh /piwigo_database_create.sh
RUN chmod +x /piwigo_database_create.sh

# Add script that can be run using docker exec to change certain piwigo defaults after install
COPY config/piwigo_database_apply_default_settings.sh /piwigo_database_apply_default_settings.sh
RUN chmod +x /piwigo_database_apply_default_settings.sh
          
# Download and install latest version of piwigo to apache web folder
RUN curl http://piwigo.org/download/dlcounter.php?code=latest | bsdtar -xvf- -C /var/www

# And some plygins (these will need manual activation in piwigo app)
RUN curl http://piwigo.org/ext/download.php?rid=5869 | bsdtar -xvf- -C /var/www/piwigo/plugins

# Add custom piwigo config file
COPY config/config.inc.php /var/www/piwigo/local/config/

# Create volume at default piwigo gallery location
VOLUME /var/www/piwigo/upload

# Execute start script to finish configuration and run mariadb and apache2
# web server
ENTRYPOINT ["/start.sh"]
