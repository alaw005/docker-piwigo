#!/bin/bash

# Set piwigo table based settings
# select left(param,50), left(value,50), left(comment,50) from piwigo.piwigo_config;
mysql -u root <<'EOF'
    UPDATE piwigo.piwigo_config SET value = false WHERE param = 'activate_comments';
    UPDATE piwigo.piwigo_config SET value = 'Just another Piwigo gallery' WHERE param = 'gallery_title';
    UPDATE piwigo.piwigo_config SET value = false WHERE param = 'allow_user_registration'; 
    UPDATE piwigo.piwigo_config SET value = true WHERE param = 'email_admin_on_new_user';
    UPDATE piwigo.piwigo_config SET value = true WHERE param = 'obligatory_user_mail_address'; 
EOF
