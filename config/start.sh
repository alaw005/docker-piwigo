#!/bin/bash

# Configure apache2 server
# Set piwigo as default application
sed -i "s/\/var\/www\/html/\/var\/www\/piwigo/g"  /etc/apache2/sites-enabled/000-default.conf

# TODO: Set ServerName and ServerAdmin email address

# Start mariadb
service mysql start

# Start apache2 server
apache2ctl -D FOREGROUND

