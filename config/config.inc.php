<?php

/* General custom settings, where different from default */
$conf['calendar_show_empty'] = false;
$conf['newcat_default_commentable'] = false;
$conf['newcat_default_status'] = 'private';
$conf['meta_ref'] = false;
$conf['show_thumbnail_caption'] = false;


/* Enable video import and metadata as part of piwigo-videojs plugin. Refer
https://github.com/xbgmsharp/piwigo-videojs/wiki/How-to-add-videos */

/* Import videos, requires videojs plugin to play */
$conf['upload_form_all_types'] = true;
$conf['sync_chars_regex'] = '/^[a-zA-Z0-9-_. ]+$/';

/* Metadata */
$conf['show_exif'] = true;
$conf['show_exif_fields'] = array(
  'Make',
  'Model',
  'ExifVersion',
  'Software',
  'DateTimeOriginal',
  'FNumber',
  'ExposureBiasValue',
  'FILE;FileSize',
  'ExposureTime',
  'Flash',
  'ISOSpeedRatings',
  'FocalLength',
  'FocalLengthIn35mmFilm',
  'WhiteBalance',
  'ExposureMode',
  'MeteringMode',
  'ExposureProgram',
  'LightSource',
  'Contrast',
  'Saturation',
  'Sharpness',
  'bitrate',
  'channel',
  'date_creation',
  'display_aspect_ratio',
  'duration',
  'filesize',
  'format',
  'formatprofile',
  'codecid',
  'frame_rate',
  'latitude',
  'longitude',
  'make',
  'model',
  'playtime_seconds',
  'sampling_rate',
  'type',
  'resolution',
  'rotation',
  );

?>
