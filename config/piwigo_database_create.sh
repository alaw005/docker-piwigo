#!/bin/bash

# Initialise mariadb and create piwigo database and user
# NB: First section avoids having to run "mysql_secure_installation"
#     Second section creates database 'piwigo' with u/p piwigo/piwigo
mysql -u root <<'EOF'
    UPDATE mysql.user SET Password=PASSWORD('piwigo') WHERE User='root';
    DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
    DELETE FROM mysql.user WHERE User='';
    DELETE FROM mysql.db WHERE Db='test' OR Db='test_%';

    CREATE DATABASE piwigo;
    GRANT ALL ON piwigo.* TO piwigo@localhost IDENTIFIED BY 'piwigo';

    FLUSH PRIVILEGES;
EOF
